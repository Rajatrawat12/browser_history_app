import logo from "./logo.svg";
import "./App.css";
import BrowserHistory from "./components/browserHistory";
import { useEffect, useState } from "react";

function App() {
  const historyList = [
    {
      id: 1,
      timeAccessed: "07:45 PM",
      logoUrl:
        "https://img.freepik.com/premium-vector/purple-gradiend-social-media-logo_197792-1883.jpg",
      title: "Instagram",
      domainUrl: "Instagram.com",
    },
    {
      id: 2,
      timeAccessed: "09:45 PM",
      logoUrl:
        "https://icon-library.com/images/facebook-icon-url/facebook-icon-url-6.jpg",
      title: "facebook",
      domainUrl: "facebook.com",
    },
    {
      id: 3,
      timeAccessed: "10:45 PM",
      logoUrl:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTq3Io3fBSQ4UAlvkjbqlXy2Q3ldKt4z-Rz9tWg3o9OsQ&s",
      title: "Gmail",
      domainUrl: "Gmail.com",
    },
    {
      id: 4,
      timeAccessed: "12:45 PM",
      logoUrl:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/WhatsApp.svg/512px-WhatsApp.svg.png?20220228223904",
      title: "whatsapp",
      domainUrl: "whatsapp.com",
    },
    {
      id: 5,
      timeAccessed: "01:35 AM",
      logoUrl:
        "https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png",
      title: "github",
      domainUrl: "github.com",
    },
    {
      id: 6,
      timeAccessed: "08:55 AM",
      logoUrl: "https://img.icons8.com/?size=1x&id=19318&format=png",
      title: "youtube",
      domainUrl: "youtubecom",
    },
    {
      id: 7,
      timeAccessed: "06:45 AM",
      logoUrl:
        "https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Telegram_logo.svg/512px-Telegram_logo.svg.png?20220101141644",
      title: "telegram",
      domainUrl: "telegram.com",
    },
    {
      id: 8,
      timeAccessed: "11:45 PM",
      logoUrl:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR8iZeszamME8SQD2EtZLCtw1guU2Dl4geAaA&usqp=CAU",
      title: "twitter",
      domainUrl: "twitter.com",
    },
    {
      id: 9,
      timeAccessed: "10:45 PM",
      logoUrl:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRhwFXc0_kkWCXzk1xUiYZbiMEGg89f60qq6Q&usqp=CAU",
      title: "google",
      domainUrl: "google.com",
    },
  ];
  const [newHistoryList, setNewHistoryList] = useState(historyList);
  const [updatedlist, SetUpdatedList] = useState(historyList);
  const [searchinput, SetSearchInput] = useState("");

  const searchedItem = (event) => {
    SetSearchInput(event.target.value);
  };
  const filteredItemLists = () => {
      const searchedList = newHistoryList.filter((eachitems) => {
        return eachitems.title.includes(searchinput);
      });
      return searchedList;
  };
  useEffect(() => {
    SetUpdatedList(filteredItemLists());
  }, [searchinput,newHistoryList]);

  const onClickDelete = (id) => {
    const filteredList = updatedlist.filter((each) => {
      return each.id !== id;
    });
    // SetUpdatedList(filteredList);
    setNewHistoryList(filteredList)
    // return filteredList;
  };
  
  return (
    <div>
      <div className="header">
        <img
          className="historyicon"
          src="https://assets.ccbp.in/frontend/react-js/history-website-logo-img.png"
        />
        <div className="searchinputicon">
          <div className="iconcontainer">
            <img
              className="searchicon"
              src="https://assets.ccbp.in/frontend/react-js/search-img.png"
            />
          </div>
          <div>
            <input
              className="inputbox"
              type="search"
              placeholder="search history"
              onChange={searchedItem}
            ></input>
          </div>
        </div>
      </div>
      {updatedlist.length>0 && <div className="viewhistorycontainer">
        {updatedlist.map((eachitem) => {
          return (
            <BrowserHistory eachitem={eachitem} onClickDelete={onClickDelete} />
          );
        })}
      </div>}
      {updatedlist.length===0 && <div className="elseviewhistorycontainer">
        <h1 className="emptymessage">There is no history to show</h1>
      </div>}
    </div>
  );
  
}


export default App;
