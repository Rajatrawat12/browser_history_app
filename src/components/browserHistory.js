import "./browserHistory.css";

const BrowserHistory = (props) => {
  const { eachitem, onClickDelete } = props;
  const { id, timeAccessed, logoUrl, title, domainUrl } = eachitem;

  const onDelete = () => {
    onClickDelete(id);
  };

  return (
    <div className="historycontainer">
      <div className="historydetails">
        <p className="timer">{timeAccessed}</p>
        <img className="imageicon" src={logoUrl} />
        <p className="sitename"> {title}</p>
        <p className="siteurl">{domainUrl}</p>
      </div>
      <div className="deletecontainer">
        <img
          className="deleteicon"
          src="https://assets.ccbp.in/frontend/react-js/delete-img.png"
          onClick={onDelete}
        />
      </div>
    </div>
  );
};
export default BrowserHistory;
